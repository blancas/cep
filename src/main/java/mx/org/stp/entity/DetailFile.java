/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.stp.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Luis Blancas
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DetailFile {
    private String fecha_pago;
    private String clave_rastreo;
    private String clave_banco_emisor;
    private String clave_banco_receptor;
    private String cuenta_beneficiario;
    private String monto_transferencia;

    @Override
    public String toString() {
        return "" + fecha_pago + "," + clave_rastreo + "," + clave_banco_emisor + "," + clave_banco_receptor + "," + cuenta_beneficiario + "," + monto_transferencia;
    }
    
}
