/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.stp.mb; 
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;
import mx.org.stp.controller.ArchivoController;
import mx.org.stp.entity.ArchivoBean;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import javax.faces.context.ExternalContext;
/**
 *
 * @author luisa
 */ 
@ManagedBean (name = "fileMB")
@RequestScoped
@Getter
@Setter
public class ArchivoMB
{
    private final FacesContext context = FacesContext.getCurrentInstance();
    private List<ArchivoBean> todos; 
    private ArchivoBean bean;
    private ArchivoController controller;
    private int    id;
    private String  nombre;
    private String  fecha_pago_i;
    private Date    fecha;
    private String  fecha_pago_f;
    private int     creado;
    private String  fecha_creacion;
    private boolean activo;
     private DefaultStreamedContent download;
    @PostConstruct
    public void init() {
        bean=new ArchivoBean();
        controller=new ArchivoController();
    }
    
    public List<ArchivoBean> getTodos()
    {    
        this.todos= controller.getAll();
        return this.todos;
    } 
    public void seleccionEditar(ArchivoBean editar )
    {
        this.bean=new ArchivoBean();
        this.bean=edicion(editar); 
        System.out.println("seleccion Editar (seleccionEditar) "+editar.toString());
    }
    public ArchivoBean edicion(ArchivoBean editar )
    {
        ArchivoBean b=new ArchivoBean();
        b.setId(editar.getId());
        b.setNombre(editar.getNombre());
        b.setFecha_pago_i(editar.getFecha_pago_i());
        b.setFecha_pago_f(editar.getFecha_pago_f());
        b.setFecha_creacion(editar.getFecha_creacion());
        b.setCreado(editar.getCreado());
        b.setActivo(editar.isActivo());
        b.setFile(editar.getFile());
        return b;
    } 
    public void AnexarUsuario(Date fecha_pago_i,int usuario)
    { 
        String rutaNombre="/home/stp/app/cep/archivosCEP/";
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(fecha_pago_i);
        System.out.println("Editar Nombre:"+this.id); 
        System.out.println("Editar Activo:"+this.activo);
        RequestContext reqcontext = RequestContext.getCurrentInstance();
        this.bean=edicion(controller.obtieneCadenayCreaArchivo(date,rutaNombre,usuario));
        reqcontext.execute("PF('EditarActivo').show();");
    }
    public void actualiza()
    { 
        try {
            RequestContext reqcontext = RequestContext.getCurrentInstance();
            reqcontext.execute("PF('EditarActivo').hide();");
            FacesContext facesContext = FacesContext.getCurrentInstance();
            facesContext.getExternalContext().redirect("Lista.xhtml?faces-redirect=true");
        } catch (IOException ex) {
            Logger.getLogger(ArchivoMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void setDownload(DefaultStreamedContent download) 
    {
        this.download = download;
    }

    public DefaultStreamedContent getDownload() throws Exception 
    {
        return download;
    }
    public void prepDownload(String date) throws Exception 
    {
        File file = new File(date);
        InputStream input = new FileInputStream(file);
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        setDownload(new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
    }
}
