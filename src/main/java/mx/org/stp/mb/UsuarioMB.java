/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.stp.mb; 
import java.util.List;
import javax.annotation.PostConstruct; 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;
import mx.org.stp.controller.PerfilController;
import mx.org.stp.controller.UsuarioController;
import mx.org.stp.entity.Perfil;
import mx.org.stp.entity.Usuario;

/**
 *
 * @author luisa
 */ 
@ManagedBean (name = "userMB")
@RequestScoped
@Getter
@Setter
public class UsuarioMB
{
    private final FacesContext context = FacesContext.getCurrentInstance();
    private List<Usuario> todos; 
    private Usuario bean;
    private UsuarioController controller;
    private int     id;
    private String  nombre;
    private String  paterno;
    private String  materno;
    private String  password1;
    private String  password2;
    private int     perfil;
    private String  login;
    private boolean activo;
    private List<Perfil> perfiles; 
    @PostConstruct
    public void init() {
        bean=new Usuario();
        controller=new UsuarioController();
    }
    public void  setPerfiles(List<Perfil> listae)
    {
        this.perfiles =listae;
    }
    public List<Perfil> getPerfiles()
    {
        PerfilController localC = new PerfilController();
        this.perfiles = localC.getAll();
        return this.perfiles;
    }
    public List<Usuario> getTodos()
    {    
        this.todos= controller.getAll();
        return this.todos;
    } 
    public void seleccionEditar(Usuario editar )
    {
        this.bean=new Usuario();
        this.bean.setId(editar.getId());
        this.bean.setNombre(editar.getNombre());
        this.bean.setApellidos(editar.getApellidos());
        this.bean.setPaterno(editar.getPaterno());
        this.bean.setMaterno(editar.getMaterno());
        this.bean.setLogin(editar.getLogin());
        this.bean.setPerfil(editar.getPerfil()); 
        this.bean.setActivo(editar.getActivo());
        System.out.println("seleccion Editar "+editar.toString());
    } 
    public void editarUsuario(int id,String nombre,
                        String paterno,String materno,
                        String login,int perfil,boolean estatus)
    { 
        this.bean=new Usuario();
        this.bean.setId(id);
        this.bean.setNombre(nombre);
        this.bean.setApellidos(paterno + " " + materno);
        this.bean.setPaterno(paterno);
        this.bean.setMaterno(materno);
        this.bean.setLogin(login);
        this.bean.setPerfil(perfil); 
        this.bean.setActivo(estatus?1:0);
        controller.editar(this.bean, 1); 
    }
    
    public void AnexarUsuario(int id)
    {
        
        controller.crea(this.login,this.password1,this.nombre,this.paterno,this.materno,this.perfil,1,id); 
    }
    public void borrarUsuario(Usuario editar,int id)
    {
        controller.borrar(editar, id);
    }
    
}
